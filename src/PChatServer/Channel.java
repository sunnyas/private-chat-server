/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PChatServer;

import java.util.ArrayList;

/**
 *
 * @author Siddharth Asnani
 */
public class Channel {

    public ChannelHandler handler;
    private ArrayList<String> chatHistory;
    public ArrayList<Client> clients;
    public String name;

    public Channel(String name) {
        clients = new ArrayList<Client>();
        chatHistory = new ArrayList<String>();
        this.name = name;
    }

    public void msgToAll(String s, String from) {
        String output = from + ":" + s;
        chatHistory.add(output);
        output = "incoming:" + output;
        for (Client c : clients) {
            if(!c.toString().equalsIgnoreCase(from)) {
                c.sendMsg(output);
            }
        }
        ProjectHandler.GUI.updateChat();
        ProjectHandler.GUI.updateUsers();
    }

    public String chatToClient() {
        String output = "";
        for (String s : chatHistory) {
            output += s + "\n";
        }
        return output;
    }

    public void kickAll() {
        for (Client c : clients) {
            c.disconnect();
        }
    }

    public String people()
    {
        String output = "user";
        for (Client c : clients) {
            output += "-" + c.toString();
        }
        return output;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
