/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PChatServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddharth Asnani
 */
public class Client implements Runnable {

    private String userName;
    public Channel myChannel;
    private Socket mySocket;
    private BufferedReader in;
    private PrintWriter out;
    private boolean loggedIn = false;

    public Client(Socket s) {
        userName = "Noob";
        mySocket = s;
        try {
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

        } catch (java.io.IOException ioe) {
            ProjectHandler.GUI.serverMsg("Error: " + ioe.getMessage());
        //ioe.printStackTrace();
        }
    }

    public void run() {
        boolean crash = false;
        try {
            while (mySocket.isConnected()) {
                String line = in.readLine();
                System.out.println(userName + ":" + line);
                commands(line);
                if (myChannel == null && line != null && loggedIn) {
                    commands("join:Lobby");
                }
                if (line == null) {
                    mySocket.close();
                    commands("leave");
                    ProjectHandler.clients.clients.remove(this);
                    return;
                }
            }
        } catch (IOException e) {
            ProjectHandler.GUI.serverMsg("Client: " + userName + " disconnected.");
            System.out.println("Error:" + e.getMessage());
            crash = true;
        }


        try {
            synchronized (ProjectHandler.clients.clients) {
                ProjectHandler.clients.clients.remove(this);
                if (myChannel == null) {
                    return;
                } else {
                    myChannel.clients.remove(this);
                    myChannel = null;
                }
                ProjectHandler.GUI.updateUsers();
                in.close();
                out.close();
                mySocket.close();
            }
        } catch (IOException ex) {
            System.out.println("Error:" + ex.getMessage());
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (!crash) {
            ProjectHandler.GUI.serverMsg("Client: " + userName + " disconnected.");
        }
    }

    public void disconnect() {
        try {
            sendMsg("sudo:disconnect");
            mySocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void sendMsg(String msg) {
        out.println(msg);
        System.out.println("To:" + userName + ":" + msg);
    }

    public void commands(String line) {
        if (line == null) {
            disconnect();
            return;
        } else if (line.startsWith("login:")) {
            String[] splits = line.split(":");
            String[] info = splits[1].split("-");
            synchronized (ProjectHandler.clients.clients) {
                for (Client c : ProjectHandler.clients.clients) {
                    if (info[0].equalsIgnoreCase("Server") || info[0].equalsIgnoreCase(c.toString())) {
                        sendMsg("sudo:fuckyou");
                        disconnect();
                        return;
                    }
                }
            }

            if (info[1].equalsIgnoreCase(ProjectHandler.server.password)) {
                sendMsg("allowed");
                line = line.substring(6);
                userName = line.substring(0, line.indexOf('-'));
            

            } else {
                sendMsg("denied");
                disconnect();
            }

        } else if (line.startsWith("disconnected")) {
            disconnect();
        } else if (line.startsWith("join:")) {
            commands("leave");
            line = line.substring(5);
            ProjectHandler.channels.clientJoinChannel(this, line);
            ProjectHandler.GUI.serverMsg("Client: " + userName + " joined " + line);
            ProjectHandler.GUI.updateUsers();
        } else if (line.startsWith("leave")) {
            if (myChannel == null) {
                return;
            } else {
                myChannel.clients.remove(this);
                myChannel = null;
            }
            ProjectHandler.GUI.updateUsers();
        } else if (line.startsWith("make:")) {
            commands("leave");
            ProjectHandler.channels.newChannel(line.substring(5));
            commands("join:" + line.substring(5));
            commands("getChannels");
            commands("getUsers:"+line.substring(5));
        } else if (line.startsWith("getChannels")) {
            loggedIn = true;
            String output = "sudo:chanList:";
            for (Channel c : ProjectHandler.channels.channels) {
                if (c != null) {
                    output += c.name + "-";
                }
            }
            output = output.substring(0, output.length() - 1);
            sendMsg(output);
        } else if (line.startsWith("getUsers:")) {
            line = (line.split(":"))[1];
            if (line == null) {
                sendMsg("screwYou!");
            }
            for (Channel c : ProjectHandler.channels.channels) {
                if (c == null) {
                    continue;
                }
                if (c.name.equalsIgnoreCase(line)) {
                    sendMsg(c.people());
                    return;
                }
            }
            sendMsg("screwYou!");
        } else if (line.startsWith("getChat")) {
            if (myChannel == null) {
                sendMsg("Server:You are not in a channel.");
            } else {
                String output = myChannel.chatToClient();
                sendMsg(output);
            }
        } else if (line.startsWith("msg:")) {
            line = line.substring(4);
            myChannel.msgToAll(line, userName);
        } else {
            sendMsg("No command found: " + line);
        }
    }

    @Override
    public String toString() {
        return userName;
    }
}
