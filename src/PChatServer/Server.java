/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PChatServer;

import java.net.*;
import java.io.*;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddharth Asnani
 */
public class Server implements Runnable {

    public Server() {
    }
    public int port = 13370;
    /** Main thread for Server */
    public Thread mainloop;
    public ServerGUI GUI;
    /** Whether the server is online or not */
    public boolean online = false;
    public ServerSocket serverSocket = null;
    public Socket clientSocket = null;
    public ChannelHandler channelHandler;
    public ClientHandler clientHandler;
    public String password;

    public void startServer() {
        online = true;
        GUI = ProjectHandler.GUI;
        password = GUI.password.getText();
        channelHandler = new ChannelHandler();
        ProjectHandler.channels = channelHandler;
        ProjectHandler.server = this;
        clientHandler = new ClientHandler();
        ProjectHandler.clients = clientHandler;
        mainloop = new Thread(this);
        mainloop.start();
        new ProjectHandler().startUpdates();
        ProjectHandler.GUI.lastSelected = channelHandler.channels[0];
    }

    @SuppressWarnings("deprecation")
    public void stopServer() {
        try {
            online = false;
            serverSocket.close();
            ProjectHandler.kick = true;
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            GUI.serverMsg("Server started, and listening on port " + GUI.server.port);
        } catch (IOException e) {
            GUI.serverMsg("Could not listen on port: " + port);
        }
        online = true;
        while (online) {
            try {

                clientSocket = serverSocket.accept();
                GUI.serverMsg("Client Accepted. Address: " + clientSocket.getInetAddress().toString().substring(1));
                clientHandler.newClient(clientSocket);

            } catch (IOException e) {
                if (online) {
                    GUI.serverMsg("Accept failed, Error: " + e.getMessage());
                }
            }
        }
    }
}
            