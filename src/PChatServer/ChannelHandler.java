/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PChatServer;

/**
 *
 * @author Siddharth Asnani
 */
public class ChannelHandler {

    private final int maxChannels = 5;
    public Channel[] channels = new Channel[maxChannels];

    public ChannelHandler() {
        newChannel("Lobby");
    }

    public void clientJoinChannel(Client a, String channel) {

        for (int i = 0; i < maxChannels; i++) {
            if (channels[i] == null) {
                continue;
            }
            if (channels[i].name.equalsIgnoreCase(channel)) {
                channels[i].clients.add(a);
                a.myChannel = channels[i];
                a.sendMsg("sudo:msg:Welcome to " + channels[i].name);

                return;
            }
        }
        a.sendMsg("server:cannotfindchannel");
    }

    public void newChannel(String name) {
        int i = 0;
        int index = 0;
        boolean make = false;
        while (true) {
            if (i >= maxChannels) {
                break;
            } else if (channels[i] == null) {
                if (!make) {
                    make = true;
                    index = i;
                }
            } else if (channels[i].name.equalsIgnoreCase(name)) {
                make = false;
                break;
            }
            i++;
        }
        if (make) {
            channels[index] = new Channel(name);
            ProjectHandler.GUI.serverMsg("Channel " + name + " created.");
            channels[index].handler = this;
            updateChannelsClient();
        } else {
            ProjectHandler.GUI.serverMsg("Creating channel: " + name + " failed.");
        }
    }

    public void updateChannelsClient() {
        if (ProjectHandler.clients == null) {
            return;
        }
        for (Client c : ProjectHandler.clients.clients) {
            if (c != null) {
                c.commands("getChannels");
            }
        }
    }
}
