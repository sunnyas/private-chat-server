/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PChatServer;

import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Siddharth Asnani
 */
public class ClientHandler {

    public final int maxClients = 30;
    public ArrayList<Client> clients;

    public ClientHandler() {
        clients = new ArrayList<Client>(maxClients);
    }

    public void newClient(Socket s) {
        int i;
        Client temp = null;
        for (i = 0; i < maxClients; i++) {
            if (clients.size() <= 29) {
                synchronized (clients) {
                    temp = new Client(s);
                    clients.add(temp);
                    break;
                }

            } else {
                temp = new Client(s);
                ProjectHandler.GUI.serverMsg("Max Clients reached!");
                temp.sendMsg("denied");
                temp.disconnect();
                return;
            }
        }

        (new Thread(temp)).start();

    }

    public void killAll() {
        for (int i = 0; i < ProjectHandler.channels.channels.length; i++) {
            Channel a = ProjectHandler.channels.channels[i];
            if (a != null) {
                a.kickAll();
            }
        }
        ProjectHandler.GUI.updateChannels();
        ProjectHandler.GUI.updateUsers();
    }
}
