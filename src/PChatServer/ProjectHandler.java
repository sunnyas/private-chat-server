/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PChatServer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddharth Asnani
 */
public class ProjectHandler implements Runnable {

    public static ServerGUI GUI;
    public static Server server;
    public static ChannelHandler channels;
    public static ClientHandler clients;
    private Thread t;
    public static boolean kick = false;

    public void startUpdates() {
        t = new Thread(this);
        t.start();
    }

    @SuppressWarnings("static-access")
    public void run() {
        //Following Code for updating ChannelList and Chat every 10 seconds
        while (ProjectHandler.server.online) {
            try {
                if (ProjectHandler.kick) {
                    ProjectHandler.clients.killAll();
                    ProjectHandler.GUI.serverMsg("Server: Kicked All");
                    ProjectHandler.kick = false;
                }
                ProjectHandler.GUI.updateChannels();
                ProjectHandler.GUI.updateChat();
                ProjectHandler.GUI.updateUsers();
                
                synchronized (ProjectHandler.channels.channels) {
                    
                    for (int i=0;i<ProjectHandler.channels.channels.length;i++) {
                        Channel c = ProjectHandler.channels.channels[i];
                        if (c == null || c.name.equalsIgnoreCase("Lobby")) {
                            continue;
                        }
                        else if (c.clients.size()<1) {
                            System.out.println(c.name +" deleted.");
                            ProjectHandler.GUI.serverMsg(c.name +" deleted.");
                            c.kickAll();
                            ProjectHandler.channels.channels[i]=null;
                            ProjectHandler.channels.updateChannelsClient();
                            ProjectHandler.GUI.updateChannels();
                            ProjectHandler.GUI.updateChat();
                            ProjectHandler.GUI.updateUsers();
                        }
                    }
                }
                
                t.sleep(5000);
            } catch (Exception ex) {
                Logger.getLogger(ServerGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ProjectHandler.GUI.updateChannels();
        ProjectHandler.GUI.updateChat();
        ProjectHandler.GUI.updateUsers();
    }
}
